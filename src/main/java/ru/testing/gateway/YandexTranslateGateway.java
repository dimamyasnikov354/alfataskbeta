package ru.testing.gateway;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import kong.unirest.HttpResponse;
import kong.unirest.Unirest;
import lombok.extern.slf4j.Slf4j;
import ru.testing.request.TranslationRequest;
import ru.testing.response.TranslationModel;
import ru.testing.response.TranslationResponse;

import java.util.List;

@Slf4j
public class YandexTranslateGateway {

    private final String URL = "https://translate.api.cloud.yandex.net/translate/v2/translate";
    private final String API_KEY = "Api-Key " + "AQVNzdBW41Vo5ddCup30F4WyiI5cKNu57eaoVSXV";
    private final String AUTH_HEADER = "Authorization";


    public List<TranslationResponse> getTranslates(TranslationRequest translationRequest) {
        HttpResponse<String> response = Unirest.post(URL)
                .header(AUTH_HEADER, API_KEY)
                .body(translationRequest)
                .asString();

        log.info("HTTP code: {}\n>>> REQUEST IS: {}\n>>> RESPONSE IS:\n{}",
                response.getStatus(),
                new GsonBuilder().setPrettyPrinting().create().toJson(translationRequest),
                response.getBody());

        return new Gson()
                .fromJson(response.getBody(), TranslationModel.class)
                .getTranslations();
    }
}
