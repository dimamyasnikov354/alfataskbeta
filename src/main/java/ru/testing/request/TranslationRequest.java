package ru.testing.request;

import ru.testing.request.glossary.GlossaryConfig;

import java.util.List;

public class TranslationRequest {
    private final String sourceLanguageCode;
    private final String targetLanguageCode;
    private final List<String> texts;
    private final GlossaryConfig glossaryConfig;

    public TranslationRequest(String sourceLanguageCode, String targetLanguageCode, List<String> texts, GlossaryConfig glossaryConfig) {
        this.sourceLanguageCode = sourceLanguageCode;
        this.targetLanguageCode = targetLanguageCode;
        this.texts = texts;
        this.glossaryConfig = glossaryConfig;
    }
}
