package ru.testing.request.glossary;

public class GlossaryConfig {
    private final GlossaryData glossaryData;

    public GlossaryConfig(GlossaryData glossaryData) {
        this.glossaryData = glossaryData;
    }
}
