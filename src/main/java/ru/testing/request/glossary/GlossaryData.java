package ru.testing.request.glossary;

import java.util.List;

public class GlossaryData {
    private final List<GlossaryPairs> glossaryPairs;

    public GlossaryData(List<GlossaryPairs> glossaryPairs) {
        this.glossaryPairs = glossaryPairs;
    }
}
