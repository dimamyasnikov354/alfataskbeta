package ru.testing.request.glossary;

public class GlossaryPairs {
    private final String sourceText;
    private final String translatedText;

    public GlossaryPairs(String sourceText, String translatedText) {
        this.sourceText = sourceText;
        this.translatedText = translatedText;
    }
}
