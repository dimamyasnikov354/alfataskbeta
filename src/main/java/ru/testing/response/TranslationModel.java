package ru.testing.response;

import java.util.List;

public class TranslationModel {
    private final List<TranslationResponse> translations;

    public TranslationModel(List<TranslationResponse> translations) {
        this.translations = translations;
    }

    public List<TranslationResponse> getTranslations() {
        return translations;
    }
}
