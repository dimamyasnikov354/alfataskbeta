package ru.testing.response;

public class TranslationResponse {
    private final String text;
    private final String detectedLanguageCode;

    public TranslationResponse(String text, String detectedLanguageCode) {
        this.text = text;
        this.detectedLanguageCode = detectedLanguageCode;
    }

    public String getText() {
        return text;
    }

    @Override
    public String toString() {
        return "{\"TranslationResponse\":{"
                + "\"text\":\"" + text + "\""
                + ", \"detectedLanguageCode\":\"" + detectedLanguageCode + "\""
                + "}}";
    }
}
