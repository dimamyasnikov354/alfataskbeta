package ru.testing;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import ru.testing.gateway.YandexTranslateGateway;
import ru.testing.request.TranslationRequest;
import ru.testing.request.glossary.GlossaryConfig;
import ru.testing.request.glossary.GlossaryData;
import ru.testing.request.glossary.GlossaryPairs;
import ru.testing.response.TranslationResponse;

import java.util.Collections;
import java.util.List;


public class YandexTranslateTest {

    private final String sourceLanguageCode = "en";
    private final String targetLanguageCode = "ru";
    private final String textsInput = "Hello World!";
    private final String textsExpected = "Всем Привет!";
    private final String sourceText = "hello world!";
    private final String translatedText = "всем привет!";

    private final GlossaryPairs glossaryPairs = new GlossaryPairs(sourceText, translatedText);
    private final GlossaryData glossaryData = new GlossaryData(Collections.singletonList(glossaryPairs));
    private final GlossaryConfig glossaryConfig = new GlossaryConfig(glossaryData);

    TranslationRequest translationRequest = new TranslationRequest(
            sourceLanguageCode,
            targetLanguageCode,
            Collections.singletonList(textsInput),
            glossaryConfig);

    @Test
    public void getTranslateForTest() {
        YandexTranslateGateway yandexTranslateGateway = new YandexTranslateGateway();
        final List<TranslationResponse> translationResponse = yandexTranslateGateway.getTranslates(translationRequest);
        Assertions.assertEquals(textsExpected, translationResponse.get(0).getText());
    }

    @ParameterizedTest
    @CsvSource({"'en', 'ru', 'Hello World!', 'Всем Привет!', 'hello world!', 'всем привет!'",
            "'en', 'ru', 'Hello World!', 'Всем Привет!', '', ''",
            "'', 'ru', 'Hello World!', 'Привет, Мир!', '', ''",
            "'en', '', 'Hello World!', 'Всем Привет!', '', ''",
            "1, 2, 'Hello World!', 'Всем Привет!', '', ''",
            "'ru', 'uk', 'Вертолет', 'Гвинтокрил', '', ''",
            "'ru', 'uk', 'Вертолет', 'Вертоліт', '', ''",
            "'ru', 'uk', 1, 1, 2, '2'",
            "'ru', 'uk', 1, 1, '2', '2'",
            "'ru', 'uk', 1, 1, '2', 2"})
    public void getTranslateForParametrizedTest(String sourceLanguageCode,
                                                String targetLanguageCode,
                                                String textsInput,
                                                String textsExpected,
                                                String sourceText,
                                                String translatedText) {

        GlossaryPairs glossaryPairs = new GlossaryPairs(sourceText, translatedText);
        GlossaryData glossaryData = new GlossaryData(Collections.singletonList(glossaryPairs));
        GlossaryConfig glossaryConfig = new GlossaryConfig(glossaryData);

        TranslationRequest translationRequest = new TranslationRequest(
                sourceLanguageCode,
                targetLanguageCode,
                Collections.singletonList(textsInput),
                glossaryConfig);

        YandexTranslateGateway yandexTranslateGateway = new YandexTranslateGateway();
        final List<TranslationResponse> translationResponse = yandexTranslateGateway.getTranslates(translationRequest);
        Assertions.assertEquals(textsExpected, translationResponse.get(0).getText());
    }
}
